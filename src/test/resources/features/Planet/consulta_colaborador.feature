
@Historia_de_usuario
Feature: Formulario Búsqueda Colaborador Choucair
  El usuario debe poder diligenciar los datos requeridos en el formulario de búsqueda de la compañia.
  Se diligencian todos los filtros de búsqueda para encontrar un colaborador de Choucair 
  y confirmar sus datos básicos (email, cargo y ubicación).

  @CasoExitoso
  Scenario: Búsqueda exitosa de colaborador en directorio Choucair,
  se obtiene consulta de información básica.
    Given Realizar autenticación en Página Planet Choucair con usuario "mgalindo" y contraseña "S@msung2709" 
    And Ingresar a la funcionalidad Encuéntrame 
    When Diligenciar formulario de búsqueda de colaborador
    | nombre | apellidos | cargo | cliente | pais | ciudad |
    | Manuel Fernando | Galindo Ortiz | Analista de Pruebas |   | Colombia | Medellín |
    Then Confirmar resultado de busqueda 
    And Extraer datos básicos del empleado
    