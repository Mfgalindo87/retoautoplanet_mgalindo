package com.mgalindo.reto;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/features/Planet/consulta_colaborador.feature", tags = "@CasoExitoso")

public class RunTags {

}
