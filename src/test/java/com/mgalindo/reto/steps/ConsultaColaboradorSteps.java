package com.mgalindo.reto.steps;

import com.mgalindo.reto.pageobjects.PlanetLoginPage;
import com.mgalindo.reto.pageobjects.PlanetMenuPage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class ConsultaColaboradorSteps {
	
	PlanetLoginPage PlanetLoginPage;
	PlanetMenuPage PlanetMenuPage;
		
	@Step
	public void login_planet(String Usuario, String Contraseña) {
		PlanetLoginPage.open();
		PlanetLoginPage.ingresar_datos(Usuario, Contraseña);
		PlanetLoginPage.valida_principal();
		Serenity.takeScreenshot();
	}
	
	@Step
	public void ingresar_formulario_busqueda() {
		PlanetMenuPage.menu_formBusqueda();
		Serenity.takeScreenshot();
	}

}
