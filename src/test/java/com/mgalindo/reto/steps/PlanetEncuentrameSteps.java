package com.mgalindo.reto.steps;

import java.util.List;

import com.mgalindo.reto.pageobjects.PlanetEncuentramePage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class PlanetEncuentrameSteps {
	
	PlanetEncuentramePage PlanetEncuentramePage;
	
	@Step
	public void diligenciar_formulario_datos_tabla(List<List<String>> data, int id) {
		PlanetEncuentramePage.nombre(data.get(id).get(0).trim());
		PlanetEncuentramePage.apellidos(data.get(id).get(1).trim());
		PlanetEncuentramePage.cargo(data.get(id).get(2).trim());
		PlanetEncuentramePage.cliente(data.get(id).get(3).trim());
		PlanetEncuentramePage.pais(data.get(id).get(4).trim());
		PlanetEncuentramePage.ciudad(data.get(id).get(5).trim());
		PlanetEncuentramePage.buscar();
		Serenity.takeScreenshot();
		
	}
		
	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		PlanetEncuentramePage.validacionDatos();
	}
	
	@Step
	public void extraer_datos_colaborador() {
		PlanetEncuentramePage.extraccionDatos();
	}


}
