package com.mgalindo.reto.definition;

import java.util.List;

import com.mgalindo.reto.steps.ConsultaColaboradorSteps;
import com.mgalindo.reto.steps.PlanetEncuentrameSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ConsultaColaboradorDefinition {
	
	@Steps
	ConsultaColaboradorSteps ConsultaColaboradorSteps;
	@Steps
	PlanetEncuentrameSteps PlanetEncuentrameSteps;
	
	
	@Given("^Realizar autenticación en Página Planet Choucair con usuario \"([^\"]*)\" y contraseña \"([^\"]*)\"$")
	public void realizar_autenticación_en_Página_Planet_Choucair_con_usuario_y_contraseña(String Usuario, String Contraseña) {
		ConsultaColaboradorSteps.login_planet(Usuario, Contraseña);	    
	}

	@Given("^Ingresar a la funcionalidad Encuéntrame$")
	public void ingresar_a_la_funcionalidad_Encuéntrame() {
		ConsultaColaboradorSteps.ingresar_formulario_busqueda();	    
	}

	@When("^Diligenciar formulario de búsqueda de colaborador$")
	public void diligenciar_formulario_de_búsqueda_de_colaborador(DataTable dtDatosFormulario) {
		List<List<String>> data = dtDatosFormulario.raw();
		
			for (int i=1; i<data.size(); i++) {
				PlanetEncuentrameSteps.diligenciar_formulario_datos_tabla(data, i);
			try {
				Thread.sleep(5000);
			} catch(InterruptedException e) {}
		}	   
	}

	@Then("^Confirmar resultado de busqueda$")
	public void confirmar_resultado_de_busqueda() {
		PlanetEncuentrameSteps.verificar_ingreso_datos_formulario_exitoso();
		try {
			Thread.sleep(5000);
		} catch(InterruptedException e) {}
	}

	@Then("^Extraer datos básicos del empleado$")
	public void extraer_datos_básicos_del_empleado() {
		PlanetEncuentrameSteps.extraer_datos_colaborador();
	}

}
