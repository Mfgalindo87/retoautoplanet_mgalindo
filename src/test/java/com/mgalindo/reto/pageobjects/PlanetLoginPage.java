package com.mgalindo.reto.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://planet.choucairtesting.com/bin/login/Main/WebHome?origurl=/")
public class PlanetLoginPage extends PageObject{
	
	// Campo Usuario
		@FindBy(xpath="//*[@id=\'foswikiLogin\']/div/form/div[2]/p[1]/input")
		public WebElementFacade txtUsuario;
		
	// Campo Contraseña
		@FindBy(xpath="//*[@id=\'foswikiLogin\']/div/form/div[3]/p[2]/input")
		public WebElementFacade txtContraseña;

	// Boton
		@FindBy(xpath="//*[@id=\'foswikiLogin\']/div/form/div[4]/input")
		public WebElementFacade btnIniciar;

	// Label de pagina principal a validar
		@FindBy(xpath="//*[@id=\"patternSideBarContents\"]/div[1]")
		public WebElementFacade lblPaginaPpal;
		
		public void ingresar_datos(String Usuario, String Contraseña) {
			Serenity.takeScreenshot();
			txtUsuario.sendKeys(Usuario);
			txtContraseña.sendKeys(Contraseña);
			Serenity.takeScreenshot();
			btnIniciar.click();
		}
		
		public void valida_principal() {
			String labelv = "Bienvenido";
			String strMensaje = lblPaginaPpal.getText();
			assertThat (strMensaje, containsString(labelv));
		}

}
