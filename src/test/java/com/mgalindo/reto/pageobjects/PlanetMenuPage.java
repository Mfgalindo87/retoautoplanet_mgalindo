package com.mgalindo.reto.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class PlanetMenuPage extends PageObject {
	
// Menú Encuéntrame
	@FindBy(xpath="//*[@id=\'patternSideBarContents\']/span[7]/a")
	public WebElement menuEncuentrame;
	
// Submenú Empleados
	@FindBy(xpath="//*[@id=\'patternMainContents\']/div[3]/div[1]/ul/li[1]/a")
	public WebElement menuEmpleados;

// Label informativo
	@FindBy(xpath="//*[@id=\'patternMainContents\']/div[3]/div[1]/h1[1]")
	public WebElement lblFormBusqueda;
	
	public void menu_formBusqueda() {
		menuEncuentrame.click();
		Serenity.takeScreenshot();
		menuEmpleados.click();
		String Mensaje = lblFormBusqueda.getText();
		assertThat (Mensaje, containsString("Empleados Choucair"));
	}

}
