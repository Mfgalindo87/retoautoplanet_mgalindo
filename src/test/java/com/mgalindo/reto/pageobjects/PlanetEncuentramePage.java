package com.mgalindo.reto.pageobjects;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PlanetEncuentramePage extends PageObject {
	
	// Campo Nombre
		@FindBy(xpath="//*[@id=\'nombre\']")
		public WebElementFacade txtNombre;
				
	// Campo Apellidos
		@FindBy(xpath="//*[@id=\'apellidos\']")
		public WebElementFacade txtApellidos;
			
	// Campo Cargo
		@FindBy(xpath="//*[@id=\'cargo\']")
		public WebElementFacade cmbCargo;

	// Campo Cliente
		@FindBy(xpath="//*[@id=\'cliente\']")
		public WebElementFacade cmbCliente;
			
	// Campo País
		@FindBy(xpath="//*[@id=\'pais\']")
		public WebElementFacade cmbPais;
			
	// Campo Ciudad
		@FindBy(xpath="//*[@id=\'ciudad\']")
		public WebElementFacade cmbCiudad;
			
	// Campo Buscar
		@FindBy(xpath="//*[@id=\'patternMainContents\']/div[3]/div[1]/div[1]/table/tbody/tr[4]/td/a")
		public WebElementFacade btnBuscar;
			
	// Enlace Datos
		@FindBy(xpath="//*[@id=\'searchresult\']/tr/td[2]/a")
		public WebElementFacade btnValDatos;
		
					
		public void nombre(String datoPrueba) {
			txtNombre.click();
			txtNombre.clear();
			txtNombre.sendKeys(datoPrueba);
		}
		
		public void apellidos(String datoPrueba) {
			txtApellidos.click();
			txtApellidos.clear();
			txtApellidos.sendKeys(datoPrueba);
		}
			
		public void cargo(String datoPrueba) {
			cmbCargo.click();
			cmbCargo.selectByVisibleText(datoPrueba);
		}
		
		public void cliente(String datoPrueba) {
			cmbCliente.click();
			cmbCliente.selectByVisibleText(datoPrueba);
		}
		
		public void pais(String datoPrueba) {
			cmbPais.click();
			cmbPais.selectByVisibleText(datoPrueba);
		}
		
		public void ciudad(String datoPrueba) {
			cmbCiudad.click();
			cmbCiudad.selectByVisibleText(datoPrueba);
		}
			
		public void buscar() {
			btnBuscar.click();
		}
		
		public void validacionDatos() {
			btnValDatos.click();
		}
		
		public void extraccionDatos() {
		for (String winHandle : getDriver().getWindowHandles()) {
            getDriver().switchTo().window(winHandle); 
            Serenity.takeScreenshot();
		}     
      }
				
}
